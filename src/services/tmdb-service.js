
export default class TmdbService {

  static BASE = 'https://api.themoviedb.org/3';

  static DISCOVER = '/discover/movie';

  static API_KEY = '1821c6b6049945b0e08619035590d15b';

  static async fetchShowsForYear(year,sortBy,page) {

    const url = `${TmdbService.BASE}${TmdbService.DISCOVER}?api_key=${TmdbService.API_KEY}&primary_release_year=${year}&sort_by=${sortBy}&page=${page}&include_adult=false`

    try {
      const response = await fetch(url);
      const json = await response.json();
      return json;
    }
    catch(e){
      console.error(`Fetch movies failed with ${e}`)
      return { error: e };
    }
  };

  static makePosterUrl(image) {

    const imageBase = 'http://image.tmdb.org/t/p/';

    const posterSize = 'w92';

    return `${imageBase}${posterSize}${image}`;
  }
};
