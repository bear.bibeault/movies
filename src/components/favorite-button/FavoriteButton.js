import React, {useContext} from 'react';
import PropTypes from "prop-types";
import FavoritesContext from "../favorites-context/FavoritesContext";

import favoriteIcon from './favorite.svg';
import notFavoriteIcon from './not-favorite.svg';

const FavoriteButton = ({id}) => {

  const favoritesContext = useContext(FavoritesContext);

  const isFavorite = favoritesContext.favorites[id];

  const toggleFavorite = () => {
    isFavorite
      ? favoritesContext.removeFavorite(id)
      : favoritesContext.addFavorite(id);
  }

  return (
    <img src={isFavorite ? favoriteIcon : notFavoriteIcon}
         onClick={toggleFavorite}
         alt="toggle favorite"
         title={isFavorite ? 'remove as a favorite' : 'mark as a favorite'}/>
  );

};

FavoriteButton.propTypes = {
  id: PropTypes.number.isRequired,
};

export default FavoriteButton;