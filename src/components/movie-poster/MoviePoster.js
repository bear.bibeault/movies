import React, {useState} from 'react';
import PropTypes from 'prop-types';
import TmdbService from "../../services/tmdb-service"

import defaultPoster from './default-poster.png';

const MoviePoster = ({image}) => {

  const [ posterUrl, setPosterUrl ] = useState(TmdbService.makePosterUrl(image));

  const useDefault = () => {
    setPosterUrl(defaultPoster);
  }

  return (
    <img src={posterUrl} alt='movie poster' onError={useDefault}/>
  );

};

MoviePoster.propTypes = {
  image: PropTypes.string.isRequired,
}

export default MoviePoster;
