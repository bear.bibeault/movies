import React from 'react';
import FavoritesContext from './FavoritesContext';

class FavoritesProvider extends React.Component {

  state = {
    favorites: {
    }
  };

  render() {
    return (
      <FavoritesContext.Provider
        value={{
          favorites: this.state.favorites,

          addFavorite: id => {
            const favorites = Object.assign({}, this.state.favorites);
            favorites[id] = true;
            this.setState({
              favorites
            });
          },

          removeFavorite: id => {
            const favorites = Object.assign({}, this.state.favorites);
            delete favorites[id];
            this.setState({
              favorites
            });
          },
        }}
      >
        {this.props.children}
      </FavoritesContext.Provider>
    );
  }
}

export default FavoritesProvider;
