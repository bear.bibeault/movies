import React, {useState} from 'react';
import PropTypes from "prop-types";
import {Container,Row,Col,Button} from "reactstrap"
import MoviePoster from "../movie-poster/MoviePoster"
import './MoviePane.scss'
import MovieDetails from "./MoviesDetail"

const MoviePane = ({movie}) => {

  const [ expanded, setExpanded ] = useState(false);

  return (
    <Container className="movie-pane">
      <Row>

        <Col xs="2">
          <MoviePoster image={movie.poster_path}/>
        </Col>

        <Col>
          <h2>{movie.title}</h2>

          <div>
            <label>Released:</label>
            {movie.release_date}
          </div>

          <div>
            <label>Popularity:</label>
            {movie.popularity}
          </div>

          <div>
            {
              expanded
                ? <>
                    <MovieDetails movie={movie}/>
                    <Button color="primary" onClick={() => { setExpanded(false); }}>
                      ... less
                    </Button>
                  </>
                : <Button color="primary" onClick={() => { setExpanded(true); }}>
                    more...
                  </Button>
            }
          </div>

        </Col>

      </Row>

    </Container>
  );
}

MoviePane.propTypes = {
  movie: PropTypes.any.isRequired,
}

export default MoviePane;
