import React, {useContext} from 'react';
import PropTypes from "prop-types";
import {Col,Container,Row} from "reactstrap"
import FavoriteButton from "../favorite-button/FavoriteButton"

const MovieDetails = ({movie}) => {

  return (
    <Container className="movie-details">
      <Row>
        <Col>
          {movie.overview}
        </Col>
        <Col xs={2}>
          <FavoriteButton id={movie.id} />
        </Col>
      </Row>

    </Container>
  );

};

MovieDetails.propTypes = {
  movie: PropTypes.any.isRequired,
}

export default MovieDetails;
