import React, {useState, useEffect} from 'react';
import TmdbService from "../../services/tmdb-service"
import MoviePane from "./MoviePane"
import {Alert} from "reactstrap"

const MoviesList = () => {

  const [ movies, setMovies ] = useState([]);

  const [ failed, setFailed ] = useState(false);

  useEffect(() => {
    async function fetchMovies() {
      const { results, error } = await TmdbService.fetchShowsForYear(2016,'popularity.desc',1);
      if (error) {
        setFailed(true);
      }
      else {
        setFailed(false);
        setMovies(results);
      }
    }
    fetchMovies();
  },[])

  return (
    <div className="movies-list">
      {
        failed
          ? <Alert color="danger">A problem occurred while fetching the list of movies. Please try again later...</Alert>
          : movies.length
            ? movies.map(movie => {
                return (
                  <MoviePane movie={movie} key={movie.id}/>
                )
              })
            : <Alert color="secondary">No movies to show</Alert>
      }
    </div>
  );

};

export default MoviesList;
