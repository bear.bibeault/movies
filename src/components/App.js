import React from 'react';

import logo from './logo.svg';
import './App.scss';
import MoviesList from "./movies/MoviesList"
import FavoritesProvider from "./favorites-context/FavoritesProvider"

function App() {

  return (
    <FavoritesProvider>
      <div className="app">

        <header className="app-header">
          <img src={logo} className="app-logo" alt="logo"/>
          <h1>Bear's Favorite Movies</h1>
        </header>

        <MoviesList/>

      </div>
    </FavoritesProvider>
  );

}

export default App;
